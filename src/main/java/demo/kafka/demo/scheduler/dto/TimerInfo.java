package demo.kafka.demo.scheduler.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class TimerInfo implements Serializable {
    private String infoId;
    private int totalFireCount;
    private int remainingFireCount;
    private boolean runForever;
    private long repeatIntervalMs;
    private long initialOffsetMs;
    private String callbackData;
}
