package demo.kafka.demo.scheduler.controller;

import demo.kafka.demo.scheduler.dto.TimerInfo;
import demo.kafka.demo.scheduler.service.PlaygroundService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/timer")
public class PlaygroundController {
    @Autowired
    private PlaygroundService playgroundService;

    @PostMapping("/create/job")
    public void createJob(@RequestBody TimerInfo timerInfo) {
        playgroundService.createJob(timerInfo);
    }

    @GetMapping("/get-all-running-timers")
    public List<TimerInfo> getAllRunningTimers() {
        return playgroundService.getAllRunningTimers();
    }

    @GetMapping("/{timerId}")
    public TimerInfo getRunningTimerById(@PathVariable String timerId) {
        return playgroundService.getRunningTimerById(timerId);
    }

    @DeleteMapping("/delete/{timerId}")
    public boolean deleteTimerById(@PathVariable String timerId) {
        return playgroundService.deleteTimer(timerId);
    }
}
