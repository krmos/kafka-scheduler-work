package demo.kafka.demo.scheduler.service;

import demo.kafka.demo.scheduler.dto.TimerInfo;
import demo.kafka.demo.scheduler.jobs.KafkaJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlaygroundService {
    @Autowired
    private SchedulerService schedulerService;

    public void createJob(TimerInfo timerInfo) {
        schedulerService.scheduler(KafkaJob.class, timerInfo);
    }

    public boolean deleteTimer(String timerId) {
        return schedulerService.deleteTimer(timerId);
    }

    public List<TimerInfo> getAllRunningTimers() {
        return schedulerService.getAllRunningTimers();
    }

    public TimerInfo getRunningTimerById(String id) {
        return schedulerService.getRunningTimerById(id);
    }
}
