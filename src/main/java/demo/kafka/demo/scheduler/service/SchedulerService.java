package demo.kafka.demo.scheduler.service;

import demo.kafka.demo.scheduler.configuration.TimeUtils;
import demo.kafka.demo.scheduler.dto.TimerInfo;
import demo.kafka.demo.scheduler.jobs.KafkaJob;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class SchedulerService {
    private static final Logger LOG = LoggerFactory.getLogger((KafkaJob.class));

    @Autowired
    private Scheduler scheduler;

    public TimerInfo getRunningTimerById(String timerId) {
        try {
            final JobDetail jobDetail = scheduler.getJobDetail(new JobKey(timerId));
            if (jobDetail == null) {
                return null;
            }
            return (TimerInfo) jobDetail.getJobDataMap().get(timerId);
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
            return null;
        }
    }

    public void updateTimer(String timerId, TimerInfo info) {

        try {
            final JobDetail jobDetail = scheduler.getJobDetail(new JobKey(timerId));
            if (jobDetail == null) {
                LOG.error("Failed to find timer with ID {}", timerId);
                return;
            }
            jobDetail.getJobDataMap().put(timerId, info);
            scheduler.addJob(jobDetail, true, true);
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public boolean deleteTimer(String timerId) {
        try {
            return scheduler.deleteJob(new JobKey(timerId));
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
            return false;
        }
    }

    public void scheduler(Class jobId, final TimerInfo info) {
        final JobDetail jobDetail = TimeUtils.buildJobDetail(jobId, info);
        final Trigger trigger = TimeUtils.buildTrigger(jobId, info);

        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public List<TimerInfo> getAllRunningTimers() {
        try {
            return scheduler.getJobKeys(GroupMatcher.anyGroup()).stream()
            .map(jobKey -> {
                try {
                    JobDetail jobDetail = scheduler.getJobDetail(jobKey);
                    return (TimerInfo) jobDetail.getJobDataMap().get(jobKey.getName());
                } catch (SchedulerException e) {
                    LOG.error(e.getMessage(), e);
                    return null;
                }
            }).filter(Objects::nonNull)
            .collect(Collectors.toList());
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
            return Collections.emptyList();
        }

    }

    @PostConstruct
    public void init() {
        try {
            scheduler.start();
            scheduler.getListenerManager().addTriggerListener(new SimpleTriggerListener(this));
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @PreDestroy
    public void preDestroy() {
        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}

