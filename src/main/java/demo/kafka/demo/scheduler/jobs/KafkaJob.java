package demo.kafka.demo.scheduler.jobs;

import demo.kafka.demo.kafka.embadded.MessageProducer;
import demo.kafka.demo.scheduler.dto.TimerInfo;
import demo.kafka.demo.spring.kafka.Greeting;
import demo.kafka.demo.spring.kafka.KafkaApplication;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.concurrent.CountDownLatch;


@Component
public class KafkaJob implements Job {
    private static final Logger LOG = LoggerFactory.getLogger((KafkaJob.class));

    @Autowired
    private MessageProducer messageProducer;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        JobDataMap jobDataMap = jobExecutionContext.getJobDetail().getJobDataMap();
        TimerInfo info = (TimerInfo) jobDataMap.get(KafkaJob.class.getSimpleName());
        if (info != null){
            messageProducer.sendMessage(info.getCallbackData());
        }
    }
}
