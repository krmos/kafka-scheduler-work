package demo.kafka.demo.scheduler.configuration;

import demo.kafka.demo.kafka.embadded.MessageProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigApp {

    @Bean
    MessageProducer messageProducer() {
        return new MessageProducer();
    }
}
