package demo.kafka.demo.scheduler.configuration;

import demo.kafka.demo.spring.kafka.KafkaApplication;
import org.springframework.boot.autoconfigure.quartz.QuartzDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

public class QuartzConfiguration {
    @Bean
    @QuartzDataSource
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource quartzDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public KafkaApplication.MessageProducer messageProducer() {
        return new KafkaApplication.MessageProducer();
    }

    @Bean
    public KafkaApplication.MessageListener messageListener() {
        return new KafkaApplication.MessageListener();
    }
}
